# Git Summary:
Git is a distributed version-control system for tracking changes in source code during software development. It is designed for coordinating work among programmers
# Git Features:
- **Repository**:A repository is the collection of files and folders (code files) that you’re using git to track.

- **Commit**:Equivalent to saving your current work.

- **Push**:Syncing commits to your Repo.

- **Branch**: These are separate instances of the code that is different from the main codebase.

- **Merge**:Integration of two Branches.

- **Clone**: Making an exact copy of an online repository on your local machine.

- **Fork**: Forking is a lot like cloning, only instead of making a duplicate of an existing repo on your local machine, you get an entirely new repo of that code under your own name.

# Running Git:
The below image shows you the basic commands to move your files into different trees in your repository.
![git](https://gitlab.com/iotiotdotin/project-internship/orientation/-/wikis/extras/Git.png) 