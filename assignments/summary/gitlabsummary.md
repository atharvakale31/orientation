# Gitlab Summary:
- GitLab is a Git-based repository manager and a powerful complete application for software development.
 ## GitLab Workflow:
 - The GitLab Workflow is a logical sequence of possible actions to be taken during the entire lifecycle of the software development process, using GitLab as the platform that hosts your code.

 -  ### Stages of Software Development:
 - **IDEA**: Every new proposal starts with an idea, which usually come up in a chat.

- **ISSUE**: discuss an idea by creating an issue for it.team and collaborators can help you to polish and improve it in the issue tracker.

- **PLAN**: prioritize and organize the workflow in the Issue Board.

- **CODE**: write the code.

- **COMMIT**:commit the code to a feature-branch with version control.

- **TEST**: With GitLab CI, run the scripts to build and test our application.

- **REVIEW**: Once script works and tests and builds succeeds,  get the code reviewed and approved.

- **STAGING**:  deploy the code to a staging environment to check if everything worked as expected or if still adjustments are needed.